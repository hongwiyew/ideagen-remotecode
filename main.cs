using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

public class main
{
    // Start is called before the first frame update
    public void Start()
    {
        //print(Calculate("1 + 1"));
        //print(Calculate("2 * 2"));
        //print(Calculate("1 + 2 + 3"));
        //print(Calculate("6 / 2"));
        //print(Calculate("11 + 23"));
        //print(Calculate("11.1 + 23"));
        //print(Calculate("1 + 1 * 3"));

        //print(MetamorphicCalculate("( 11.5 + 15.4 ) + 10.1"));
        //print(Calculate("( 11.5 + 15.4 ) + 10.1"));
        //print(MetamorphicCalculate("23 - ( 29.3 - 12.5 )"));
        //print(Calculate("23 - ( 29.3 - 12.5 )"));
        //print(MetamorphicCalculate("10 - ( 2 + 3 * ( 7 - 5 ) )"));
        print(Calculate("10 - ( 2 + 3 * ( 7 - 5 ) )")); // nested bracket 0
        print(MetamorphicCalculate("10 - ( 2 + 3 * ( 7 - 5 ) + ( ( 7 + 3 ) - 5 ) + ( 7 - 5 ) )"));
        print(Calculate("10 - ( 2 + 3 * ( 7 - 5 ) + ( ( 7 + 3 ) - 5 ) + ( 7 - 5 ) )")); // nested bracket 1
        print(MetamorphicCalculate("10 - ( 2 + 3 * ( 7 - 5 ) + ( ( 7 + 3 * ( 2 + 6 ) ) - 5 ) + ( 7 - 5 ) )"));
        print(Calculate("10 - ( 2 + 3 * ( 7 - 5 ) + ( ( 7 + 3 * ( 2 + 6 ) ) - 5 ) + ( 7 - 5 ) )")); // nested bracket 2

        //print(Calculate("1 + a + 1")); // contain alphabets
        //print(Calculate("1 + + 1")); // invalid operator
        //print(Calculate("2 * ( ( ) 2")); // invalid brackets defined
    }
    // calculate function .Net library provided
    public static double MetamorphicCalculate(string input)
    {
        EvaluateString(input);

        double value;
        double.TryParse(new DataTable().Compute(input, null).ToString(), out value);

        return value;
    }

    // For longer answer
    public static double Calculate(string input)
    {
        EvaluateString(input);

        return CalculateSegment(input.Split(' ').ToList<string>());
    }

    // 
    private static double CalculateSegment(List<string> inputs)
    {
        //print("**************************************");
        //string processstring = "";
        //for (int i = 0; i < inputs.Count; i++)
        //{
        //    processstring += inputs[i];
        //}
        //print("Processing: " + processstring);
        //print("**************************************");


        //if there is brackets in this one, process
        if (inputs.Contains("("))
        {
            int start = inputs.IndexOf("(") + 1;
            //int end = inputs.LastIndexOf(")");
            int bracketcount = 1;

            // TODO process inside bracket first
            for (int i = start; i < inputs.Count; i++)
            {
                if (inputs[i] == ")")
                {
                    if (--bracketcount == 0)
                    {
                        inputs[start - 1] = CalculateSegment(inputs.GetRange(start, i - start)).ToString();


                        inputs.RemoveRange(start, i - start + 1);
                        i = start;
                        start = -1; //to reset next bracket point
                        //print("**************************************");
                        //string afterbracket = "";
                        //for (int j = 0; j < inputs.Count; j++)
                        //{
                        //    afterbracket += inputs[j];
                        //}
                        //print("After Bracket: " + afterbracket);
                        //print("**************************************");
                    }
                }
                else if (inputs[i] == "(")
                {
                    bracketcount++;
                    if (start == -1)
                    {
                        start = i + 1;
                    }
                }
            }
        }

        // Start calculating
        // division
        while (inputs.Contains("/"))
        {
            int divIndex = inputs.IndexOf("/");
            double lefthandvalue;
            double righthandvalue;

            double.TryParse(inputs[divIndex - 1], out lefthandvalue);
            double.TryParse(inputs[divIndex + 1], out righthandvalue);

            double value = lefthandvalue / righthandvalue;
            inputs[divIndex - 1] = value.ToString();
            inputs[divIndex] = "NA";
            inputs[divIndex + 1] = "NA";

            inputs.RemoveAll(x => x == "NA");
        }

        // multiply
        while (inputs.Contains("*"))
        {
            int divIndex = inputs.IndexOf("*");
            double lefthandvalue;
            double righthandvalue;

            double.TryParse(inputs[divIndex - 1], out lefthandvalue);
            double.TryParse(inputs[divIndex + 1], out righthandvalue);

            double value = lefthandvalue * righthandvalue;
            inputs[divIndex - 1] = value.ToString();
            inputs[divIndex] = "NA";
            inputs[divIndex + 1] = "NA";

            inputs.RemoveAll(x => x == "NA");
        }

        //add
        while (inputs.Contains("+"))
        {
            int divIndex = inputs.IndexOf("+");
            double lefthandvalue;
            double righthandvalue;

            double.TryParse(inputs[divIndex - 1], out lefthandvalue);
            double.TryParse(inputs[divIndex + 1], out righthandvalue);

            double value = lefthandvalue + righthandvalue;
            inputs[divIndex - 1] = value.ToString();
            inputs[divIndex] = "NA";
            inputs[divIndex + 1] = "NA";

            inputs.RemoveAll(x => x == "NA");
        }

        //subject
        while (inputs.Contains("-"))
        {
            int divIndex = inputs.IndexOf("-");
            double lefthandvalue;
            double righthandvalue;

            double.TryParse(inputs[divIndex - 1], out lefthandvalue);
            double.TryParse(inputs[divIndex + 1], out righthandvalue);

            double value = lefthandvalue - righthandvalue;
            inputs[divIndex - 1] = value.ToString();
            inputs[divIndex] = "NA";
            inputs[divIndex + 1] = "NA";

            inputs.RemoveAll(x => x == "NA");
        }

        //print("**************************************");
        //string resultstring = "";
        //for (int i = 0; i < inputs.Count; i++)
        //{
        //    resultstring += inputs[i];
        //}
        //print("Result: " + resultstring);
        //print("**************************************");

        double finalval;
        double.TryParse(inputs[0], out finalval);

        return finalval;
    }

    private static void EvaluateString(string input)
    {
        #region evaluate alphabets
        List<string> inputs = input.Split(' ').ToList<string>();
        Regex r = new Regex("[a-zA-Z]");
        // if contain alphabets
        if (r.IsMatch(input))
        {
            throw new System.Exception("value not able to calculate");
        }
        #endregion

        #region evaluate numbers
        // TODO evaluate int/double/long etc

        #endregion

        #region evaluate ()
        // evaluate ()
        IEnumerable<string> inbrackets = from data in inputs where data == "(" select data;
        IEnumerable<string> outbrackets = from data in inputs where data == ")" select data;
        int totalinBrackets = inbrackets.Count();
        int totaloutBrackets = outbrackets.Count();

        if (totalinBrackets != totaloutBrackets)
        {
            throw new System.Exception("Invalid backets defined");
        }

        // TODO check brackets placed properly

        #endregion

        #region evaluate +-*/
        // evaluate +-*/
        bool previsoperator = false;
        for (int i = 0; i < input.Length; i++)
        {
            switch (input[i])
            {
                case '+':
                case '-':
                case '*':
                case '/':

                    if (previsoperator)
                    {
                        throw new System.Exception("incorrect input on index [" + i + "]: " + input[i]);
                    }

                    previsoperator = true;
                    break;

                default:
                    previsoperator = false;
                    break;
            }

        }
        #endregion
    }
}
